package com.zuitt;

import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String [] args){
        Scanner in = new Scanner(System.in);

        int num = 0;

        System.out.println("Input an integer whose factorial will be computed: ");

        try{
            num = in.nextInt();
        }
        catch(Exception e){
            System.out.println("Invalid input!");
            e.printStackTrace();
            return;
        }
        if (num < 0) {
            System.out.println("Factorial of negative numbers cannot be computed.");
            return;
        }

        int factorial = 1;
        for (int i = 1; i <= num; i++) {
            factorial *= i;

        }

        System.out.println("The factorial of " + num + " is " + factorial);

    }
}
